/*======================================================================================
 *
 *	Arquivo:	Hook.h
 *	Autor:		Marcelo Henrique
 *
 *	Descri��o:
 *		
 *		Esse m�dulo tem fun��es para realiza��o de API Hooking.
 *
 *======================================================================================
 */

#ifndef __HOOK_H__
#define __HOOK_H__

#include "common.h"

EXPORT_AS_C()

/*
 *======================================================================================
 *									HookIATCall
 *--------------------------------------------------------------------------------------
 *	Est� fun��o � usada para hookar a tabela de importa��o de um arquivo PE (exe/dll).
 *--------------------------------------------------------------------------------------
 *	Par�metros		Tipo			Descri��o
 *--------------------------------------------------------------------------------------
 *	lpModuleName	LPCTSTR			Nome do m�dulo/dll que tem a fun��o a ser hookada.
 *	lpszAPIName		LPCTSTR			Nome da fun��o a ser hookada.
 *	lpNewAddress	void *			Endere�o a fun��o que substituir� a fun��o hookada.
 *	lpOldAddress	void **			Par�metro onde � retornado o endere�o da fun��o
 *									hookada. Esse endere�o deve ser passado para a
 *									fun��o que restaura os hooks feitos por HookIATCall.
 *======================================================================================
 *									UnhookIATCall
 *--------------------------------------------------------------------------------------
 *	Est� fun��o � usada para restaurar as altera��es feitas pela fun��o anterior na IAT.
 *--------------------------------------------------------------------------------------
 *	Par�metros		Tipo			Descri��o
 *--------------------------------------------------------------------------------------
 *	lpModuleName	LPCTSTR			Nome do m�dulo/dll que tem a fun��o a ser hookada.
 *	lpszAPIName		LPCTSTR			Nome da fun��o que foi hookada.
 *	lpNewAddress	void *			Endere�o da fun��o que substitui a fun��o original
 *									no momento em que HookIATCall � chamada.
 *======================================================================================
 */
ULONG HookIATCall(const char *lpModuleName, const char *lpszAPIName, void *lpNewAddress, void **lpOldAddress, const char *lpTargetModule);
ULONG UnhookIATCall(const char *lpModuleName, const char *lpszAPIName, void *lpNewAddress, const char *lpTargetModule);

BOOL HookEATCall(const char *lpModuleName, const char *lpszAPIName, void *lpNewAddress, void **lpOldAddress);
BOOL UnhookEATCall(const char *lpModuleName, const char *lpszAPIName, void *lpOldAddress);

EXPORT_AS_C_END()

#endif // __HOOK_H__