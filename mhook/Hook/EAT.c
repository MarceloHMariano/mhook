#include "..\Hook.h"
#include "HookGlobal.h"

static BOOL HookEAT(const char *lpModuleName, const char *lpszAPIName, void *lpNewAddress, void **lpOldAddress)
{
	HMODULE hDLL = PsGetModuleBaseAddressA(lpModuleName);
	if (hDLL)
	{
		PDWORD pdwExportAddr = (PDWORD)PEModGetExportAddress(hDLL, lpszAPIName);
		if (pdwExportAddr)
		{
			//DWORD dwMainModuleBase = (DWORD)PsGetMainModuleBaseAddress();
			//DWORD dwNewAddress = (DWORD)lpNewAddress - dwMainModuleBase;
			//dwNewAddress -= (DWORD)hDLL - dwMainModuleBase;
            DWORD dwNewAddress = (DWORD)lpNewAddress - (DWORD)hDLL;

			if (lpOldAddress)
				*lpOldAddress = (void *)(*pdwExportAddr + (DWORD)hDLL);

			return HookAPIAddress(pdwExportAddr, dwNewAddress);
		}
	}

	return FALSE;
}

BOOL HookEATCall(const char *lpModuleName, const char *lpszAPIName, void *lpNewAddress, void **lpOldAddress)
{
	return HookEAT(lpModuleName, lpszAPIName, lpNewAddress, lpOldAddress);
}

BOOL UnhookEATCall(const char *lpModuleName, const char *lpszAPIName, void *lpOldAddress)
{
	return HookEAT(lpModuleName, lpszAPIName, lpOldAddress, NULL);
}