#ifndef __HOOK_GLOBAL_H__
#define	__HOOK_GLOBAL_H__

#include "..\PE.h"
#include "..\Process.h"
#include <psapi.h>

static BOOL HookAPIAddress(PDWORD lpdwProcAddress, DWORD dwNewAddress)
{
    DWORD dwOld = 0;

    if (CWA(kernel32, VirtualProtect)(lpdwProcAddress, sizeof(DWORD), PAGE_READWRITE, &dwOld))
    {
        *lpdwProcAddress = dwNewAddress;

        CWA(kernel32, VirtualProtect)(lpdwProcAddress, sizeof(DWORD), dwOld, &dwOld);
        return TRUE;
    }

    return FALSE;
}

#endif // __HOOK_GLOBAL_H__