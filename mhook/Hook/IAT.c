#include "..\Hook.h"
#include "..\Memory.h"
#include "HookGlobal.h"

static ULONG HookImports(HMODULE hModuleBase, PIMAGE_IMPORT_DESCRIPTOR lpImportDesc, DWORD dwTargetAddress, DWORD dwAddress)
{
    ULONG hooks = 0;

    while (lpImportDesc->Name)
    {
        PIMAGE_THUNK_DATA lpIAT = (PIMAGE_THUNK_DATA)(lpImportDesc->FirstThunk + (DWORD)hModuleBase);

        while (lpIAT->u1.AddressOfData)
        {
            if (!(lpIAT->u1.Ordinal & IMAGE_ORDINAL_FLAG) && lpIAT->u1.Function == dwTargetAddress)
                if (HookAPIAddress(&lpIAT->u1.Function, dwAddress))
                    hooks++;

            lpIAT++;
        }

        lpImportDesc++;
    }

    return hooks;
}

static ULONG HookIAT(DWORD dwTargetAddress, DWORD dwNewAddress, const char *lpTargetModule)
{
	PIMAGE_DATA_DIRECTORY lpDataDir = NULL;
	PIMAGE_IMPORT_DESCRIPTOR lpImportDesc = NULL;
	register ULONG uResult = 0;

	if (lpTargetModule)
	{
		HMODULE hTargetModule = PsGetModuleBaseAddressA(lpTargetModule);
		if (hTargetModule)
		{
			lpDataDir = PEModGetImageDataDirectory(hTargetModule, IMAGE_DIRECTORY_ENTRY_IMPORT);
			if (lpDataDir)
			{
				lpImportDesc = (PIMAGE_IMPORT_DESCRIPTOR)(lpDataDir->VirtualAddress + (DWORD)hTargetModule);
				if (lpImportDesc)
				{
					uResult += HookImports(hTargetModule, lpImportDesc, dwTargetAddress, dwNewAddress);
					return uResult;
                }
			}
		}
	}
	else
	{
		DWORD dwModules = PsGetCurrentProcessModuleCount();
		if (dwModules)
		{
			HMODULE *hModules = (HMODULE *)MemVAlloc(NULL, dwModules * sizeof(HMODULE), PAGE_READWRITE);
			DWORD cbNeeded = 0;

			if (CWA(psapi, EnumProcessModules)(CWA(kernel32, GetCurrentProcess)(), hModules, dwModules * sizeof(HMODULE),
					&cbNeeded))
			{
				register ULONG i;

				for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++)
				{
					lpDataDir = PEModGetImageDataDirectory(hModules[i], IMAGE_DIRECTORY_ENTRY_IMPORT);
					if (lpDataDir)
					{
						lpImportDesc = (PIMAGE_IMPORT_DESCRIPTOR)(lpDataDir->VirtualAddress + (DWORD)hModules[i]);
						if (lpImportDesc)
							uResult += HookImports(hModules[i], lpImportDesc, dwTargetAddress, dwNewAddress);
					}
				}

				return uResult;
			}

			MemVFree(hModules);
		}
	}

    return 0;
}

ULONG HookIATCall(const char *lpModuleName, const char *lpszAPIName, void *lpNewAddress, void **lpOldAddress, const char *lpTargetModule)
{
	HMODULE hDLL = PsGetModuleBaseAddressA(lpModuleName);
    if (hDLL)
    {
		DWORD dwTargetAddress = (DWORD)PEModGetExportAddress(hDLL, lpszAPIName);
        if (dwTargetAddress)
        {
            ULONG uHooks = HookIAT(dwTargetAddress, (DWORD)lpNewAddress, lpTargetModule);
			if (lpOldAddress)
				*lpOldAddress = uHooks ? (void *)dwTargetAddress : NULL;

            return uHooks;
        }
    }

    return 0;
}

ULONG UnhookIATCall(const char *lpModuleName, const char *lpszAPIName, void *lpNewAddress, const char *lpTargetModule)
{
    HMODULE hDLL;
	hDLL = PsGetModuleBaseAddressA(lpModuleName);

    if (hDLL)
    {
        DWORD dwOldAddress = (DWORD)PEModGetExportAddress(hDLL, lpszAPIName);
        if (dwOldAddress)
            return HookIAT((DWORD)lpNewAddress, dwOldAddress, lpTargetModule);
    }

    return 0;
}