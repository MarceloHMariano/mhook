#ifndef __MEMORY_H__
#define	__MEMORY_H__

#include "common.h"

EXPORT_AS_C()

#define	MEM_ALLOC_TYPE	MEM_COMMIT | MEM_RESERVE
#define	MEM_FREE_TYPE	MEM_RELEASE

#define	HEAP_HANDLE		CWA(kernel32, GetProcessHeap)()

#if defined(GCC)
#define MemZero(void *, SIZE_T) ZeroMemory(void *, SIZE_T)
#endif // GCC

extern HANDLE HeapHandle;

// LocalAlloc
__inline void *MemLAlloc(size_t size)
{
	return CWA(kernel32, LocalAlloc)(LPTR, size);
}

__inline void MemLFree(void *lpBaseAddr)
{
	CWA(kernel32, LocalFree)((HLOCAL)lpBaseAddr);
}

// HeapAlloc
__inline void *MemHAlloc(size_t size)
{
	return CWA(kernel32, HeapAlloc)(HEAP_HANDLE, HEAP_ZERO_MEMORY, size);
}

__inline size_t MemHSize(void *lpBaseAddr)
{
	return CWA(kernel32, HeapSize)(HEAP_HANDLE, 0, lpBaseAddr);
}

__inline void *MemHReAlloc(void *lpBaseAddr, size_t newSize)
{
	return CWA(kernel32, HeapReAlloc)(HEAP_HANDLE, HEAP_ZERO_MEMORY, lpBaseAddr, newSize);
}

#define	MemHGrow(base, size)	MemHReAlloc(base, MemHSize(base) + size)
#define	MemHShrink(base, size)	MemHReAlloc(base, MemHSize(base) - size);

void *MemHInsertBlock(void *lpBaseAddr, void *lpAddress, size_t blockSize, void **lpInsertAddress);

__inline void MemHFree(void *lpBaseAddr)
{
	CWA(kernel32, HeapFree)(HEAP_HANDLE, 0, lpBaseAddr);
}

// VirtualAlloc
__inline void *MemVAlloc(void *lpBaseAddr, size_t size, DWORD flProtect)
{
	return CWA(kernel32, VirtualAlloc)(lpBaseAddr, size, MEM_ALLOC_TYPE, flProtect);
}

size_t MemVSize(void *lpBaseAddr);

__inline void MemVFree(void *lpBaseAddr)
{
	CWA(kernel32, VirtualFree)(lpBaseAddr, 0, MEM_FREE_TYPE);
}

void *MemVRealloc(void *lpBaseAddr, size_t size, DWORD flProtect);

// VirtualAllocEx
__inline void *MemVAllocEx(HANDLE hProcess, void *lpBaseAddr, size_t size, DWORD flProtect)
{
	return CWA(kernel32, VirtualAllocEx)(hProcess, lpBaseAddr, size, MEM_ALLOC_TYPE, flProtect);
}

__inline void MemVFreeEx(HANDLE hProcess, void *lpBaseAddr)
{
	CWA(kernel32, VirtualFreeEx)(hProcess, lpBaseAddr, 0, MEM_FREE_TYPE);
}

#define	MemZero(addr, size)			SecureZeroMemory(addr, size)
#define	MemCopy(dest, source, size)	CopyMemory(dest, source, size)

EXPORT_AS_C_END()

#endif // __MEMORY_H__