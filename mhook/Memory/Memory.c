#include "Memory.h"

void *MemHInsertBlock(void *lpBaseAddr, void *lpAddress, size_t blockSize, void **lpInsertAddress)
{
	register void *newAddr = NULL;

	if (lpAddress >= lpBaseAddr && blockSize > 0)
	{
		size_t offset = (mbyte *)lpAddress - (mbyte *)lpBaseAddr;
		size_t oldSize = MemHSize(lpBaseAddr);
		size_t tempAllocSize = oldSize - offset;

		void *temp = (mbyte *)MemHAlloc(tempAllocSize);
		if (temp)
		{
			MemCopy(temp, lpAddress, tempAllocSize);

			newAddr = MemHGrow(lpBaseAddr, blockSize);
			if (newAddr)
			{
				mbyte *p = (mbyte *)newAddr + offset;
				
				MemZero(p, blockSize);
				MemCopy(p + blockSize, temp, tempAllocSize);

				if (lpInsertAddress)
					*lpInsertAddress = p;
			}

			MemHFree(temp);
		}
	}

	return newAddr;
}

size_t MemVSize(void *lpBaseAddr)
{
	MEMORY_BASIC_INFORMATION memInfo;
	if (CWA(kernel32, VirtualQuery)(lpBaseAddr, &memInfo, sizeof(MEMORY_BASIC_INFORMATION)))
		return memInfo.RegionSize;

	return 0;
}

void *MemVRealloc(void *lpBaseAddr, size_t size, DWORD flProtect)
{
	size_t oldSize = MemVSize(lpBaseAddr);
	if (oldSize)
	{
		void *newMem = MemVAlloc(NULL, oldSize + size, flProtect);
		if (newMem)
		{
			MemZero(newMem, oldSize + size);
			MemCopy(newMem, lpBaseAddr, oldSize);
			MemVFree(lpBaseAddr);
			return newMem;
		}
	}

	return NULL;
}