#ifndef __PE_IMAGE_H__
#define	__PE_IMAGE_H__

#include "common.h"
#include "Memory.h"
#include "PE/PE32.h"

EXPORT_AS_C()

#define	PEModGetImageDirectoryData(HMODULE, dir)	(void *)(PEModGetImageDataDirectory(HMODULE, dir)->VirtualAddress + (DWORD)HMODULE)
#define	PEModGetModuleExportDirectory(HMODULE)		(PIMAGE_EXPORT_DIRECTORY)PEModGetImageDirectoryData(HMODULE, IMAGE_DIRECTORY_ENTRY_EXPORT)
#define	PEModGetCurrentModuleBaseAddress()			PEModGetModuleBaseAddressW(NULL)
/* Retorna o cabe�alho DOS do m�dulo especificado por HMODULE. */
PIMAGE_DOS_HEADER PEModGetDOSHeader(HMODULE);

/* Retorna o cabe�alho NT do m�dulo especificado por HMODULE. */
PIMAGE_NT_HEADERS PEModGetNtHeaders(HMODULE);

/* Retorna o cabe�alho COFF do m�dulo especificado por HMODULE. */
PIMAGE_FILE_HEADER PEModGetFileHeader(HMODULE);

/* Retorna o Optional Header do m�dulo especificado por HMODULE. */
PIMAGE_OPTIONAL_HEADER PEModGetOptionalHeader(HMODULE);

/* Retorna o diret�rio especificado por nDirecotyIndex do array "ImageDataDirectory"
   contido na imagem do m�dulo especificado por HMODULE. 
*/
PIMAGE_DATA_DIRECTORY PEModGetImageDataDirectory(HMODULE hModule, BYTE nDirectryIndex);

/* Obt�m o endere�o de uma fun��o lpProcName exportada pelo m�dulo hModule */
//PDWORD __PEModGetExportAddress(HMODULE hModule, const char *lpProcName);
WORD PEModGetExportOrdinal(HMODULE hModule, const char *lpProcName);
FARPROC WINAPI PEModGetExportAddress(HMODULE hModule, const char *lpProcName);

EXPORT_AS_C_END()

#endif // __PE_IMAGE_H__