#include "PE32.h"
#include "PE.h"
#include "PEDescriptor.h"
#include "Memory.h"
#include "Utils/FileUtils.h"
#include <stdio.h>

#define	EXPAND_PE_HANDLE(argname)			MPe32Descriptor *__pehandle = (MPe32Descriptor *)argname
#define	ASSERT_ARGS(condition)				if (!condition) return false

#define	GET_PE_HANDLE()						__pehandle
#define	GET_FILE_HANDLE()					GET_PE_HANDLE()->FileHandle
#define	GET_PE_MEMORY()						GET_PE_HANDLE()->BaseAddress

#define	ALIGN_VALUE(value, alignment)		((value + alignment - 1) / alignment) * alignment
#define	MPE32_FILE_ALIGNMENT(hPE, value)	ALIGN_VALUE(value, GetNtHeaders32Address(hPE)->OptionalHeader.FileAlignment)
#define	MPE32_SECTION_ALIGNMENT(hPE, value)	ALIGN_VALUE(value, GetNtHeaders32Address(hPE)->OptionalHeader.SectionAlignment)

//---------------------------------------------------------------------------------------
#define	IMPLEMENT_SET_HEADER(type, hPE, GetHeaderAddrProc, newheader) \
	type *header; \
	if (hPE) \
	{ \
		header = GetHeaderAddrProc(hPE); \
		MemCopy(header, newheader, sizeof(type)); \
		return true; \
	} \
	return false;
//---------------------------------------------------------------------------------------
static PIMAGE_DOS_HEADER GetDosHeaderAddress(mhandle_t hPE)
{
	EXPAND_PE_HANDLE(hPE);
	return (PIMAGE_DOS_HEADER)GET_PE_MEMORY();
}
//---------------------------------------------------------------------------------------
static PIMAGE_NT_HEADERS32 GetNtHeaders32Address(mhandle_t hPE)
{
	PIMAGE_DOS_HEADER dosHdr = GetDosHeaderAddress(hPE);
	return (PIMAGE_NT_HEADERS32)((unsigned long)dosHdr + dosHdr->e_lfanew);
}
//---------------------------------------------------------------------------------------
#define	PEOffsetToRVA(hPE, offset)					((unsigned long)GetDosHeaderAddress(hPE) + offset)
#define	GetDosStubAddress(hPE)						(void *)(PEOffsetToRVA(hPE, sizeof(IMAGE_DOS_HEADER)))
#define	GetFileHeaderAddress(hPE)					(&GetNtHeaders32Address(hPE)->FileHeader)
#define	GetOptionalHeader32Address(hPE)				(&GetNtHeaders32Address(hPE)->OptionalHeader)
#define GetSectionHeaderAddress(hPE, index)			IMAGE_FIRST_SECTION(GetNtHeaders32Address(hPE)) + index
#define	GetSectionDataAddress(hPE, section)			(void *)(PEOffsetToRVA(hPE, section->PointerToRawData))
#define	GetDataDirectoryHeaderAddress(hPE, index)	(&GetOptionalHeader32Address(hPE)->DataDirectory[index])
#define	GetDataDirectoryDataAddress(hPE, directory)	(void *)RVAToVA(hPE, directory->VirtualAddress)
//---------------------------------------------------------------------------------------
static unsigned long RVAToOffset(mhandle_t hPE, unsigned long rva)
{
	unsigned int i;
	PIMAGE_SECTION_HEADER header;

	for (i = 0; i < MPe32GetSectionCount(hPE); i++)
	{
		header = GetSectionHeaderAddress(hPE, i);
		if (rva >= header->VirtualAddress && rva < header->VirtualAddress + header->Misc.VirtualSize)
			return rva - header->VirtualAddress + header->PointerToRawData;
	}

	return (unsigned long)-1;
}

static mbyte *RVAToVA(mhandle_t hPE, unsigned long rva)
{
	EXPAND_PE_HANDLE(hPE);

	mbyte *p = GET_PE_MEMORY();
	return &p[RVAToOffset(hPE, rva)];
}

static unsigned long OffsetToRVA(mhandle_t hPE, unsigned long offset)
{
	unsigned int i;
	PIMAGE_SECTION_HEADER header;

	for (i = 0; i < MPe32GetSectionCount(hPE); i++)
	{
		header = GetSectionHeaderAddress(hPE, i);
		if (offset >= header->PointerToRawData && offset < header->PointerToRawData + header->SizeOfRawData)
			return offset - header->PointerToRawData + header->VirtualAddress;
	}

	return (unsigned)-1;
}

static unsigned long VAToRVA(mhandle_t hPE, void *va)
{
	EXPAND_PE_HANDLE(hPE);
	return OffsetToRVA(hPE, (unsigned long)va - (unsigned long)GET_PE_MEMORY());
}

//---------------------------------------------------------------------------------------
static PIMAGE_IMPORT_DESCRIPTOR GetImageImportDescriptorAddress(mhandle_t hPE)
{
	PIMAGE_DATA_DIRECTORY directory = GetDataDirectoryHeaderAddress(hPE, IMAGE_DIRECTORY_ENTRY_IMPORT);
	return GetDataDirectoryDataAddress(hPE, directory);
}
//---------------------------------------------------------------------------------------
mhandle_t MPe32Open(const mchar *lpFileName, MPeOpenMode mode)
{
	DWORD dwOpenFlag = mode == omCreate ? CREATE_NEW : OPEN_EXISTING;
	HANDLE hFile = CWA(kernel32, CreateFile)(lpFileName, GENERIC_READ | GENERIC_WRITE,
		FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, dwOpenFlag, 0, NULL);

	MPe32Descriptor *descriptor = NULL;

	if (hFile != INVALID_HANDLE_VALUE)
	{
		if (mode == omOpen)
		{
			descriptor = MNewPE32Descriptor(hFile);
			//if (descriptor)
			//	return (mhandle_t)descriptor;

			CWA(kernel32, CloseHandle)(hFile);
		}
	}

	return descriptor;
}
//---------------------------------------------------------------------------------------
mbool MPe32SaveFile(mhandle_t hPE, const mchar *lpFileName)
{
	EXPAND_PE_HANDLE(hPE);

	HANDLE hFile = CWA(kernel32, CreateFile)(lpFileName, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, 
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (hFile != INVALID_HANDLE_VALUE)
	{
		size_t size = MemVSize(GET_PE_MEMORY());
		DWORD dwBytesWritten;

		CWA(kernel32, WriteFile)(hFile, GET_PE_MEMORY(), size, &dwBytesWritten, NULL);
		CWA(kernel32, CloseHandle)(hFile);

		return size == dwBytesWritten;
	}

	return false;
}
//---------------------------------------------------------------------------------------
mbool MPe32Close(mhandle_t hPE)
{
	EXPAND_PE_HANDLE(hPE);

	//if (CWA(kernel32, CloseHandle)(GET_FILE_HANDLE()))
	//{
	//	MFreePE32Descriptor(GET_PE_HANDLE());
	//	return true;
	//}

	//return false;
	MFreePE32Descriptor(GET_PE_HANDLE());
	return true;
}
//---------------------------------------------------------------------------------------
unsigned int MPe32GetDosStubSize(mhandle_t hPE)
{
	return GetDosHeaderAddress(hPE)->e_lfanew - sizeof(IMAGE_DOS_HEADER);
}
//---------------------------------------------------------------------------------------
mbool MPe32GetDosStub(mhandle_t hPE, void *lpBuffer, size_t nBufferSize)
{
	size_t realSize = MPe32GetDosStubSize(hPE);
	ASSERT_ARGS(hPE && lpBuffer && nBufferSize > 0);

	if (nBufferSize > realSize)
		nBufferSize = realSize;

	MemCopy(lpBuffer, GetDosStubAddress(hPE), nBufferSize);
	return true;
}
//---------------------------------------------------------------------------------------
mbool MPe32GetDosHeader(mhandle_t hPE, PIMAGE_DOS_HEADER lpDosHeader)
{
	PIMAGE_DOS_HEADER dosHdr;

	ASSERT_ARGS(hPE && lpDosHeader);

	dosHdr = GetDosHeaderAddress(hPE);
	if (dosHdr->e_magic == IMAGE_DOS_SIGNATURE)
	{
		MemCopy(lpDosHeader, dosHdr, sizeof(IMAGE_DOS_HEADER));
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------
mbool MPe32GetNtHeaders32(mhandle_t hPE, PIMAGE_NT_HEADERS32 lpNtHeaders)
{
	PIMAGE_NT_HEADERS32 ntHdrs;

	ASSERT_ARGS(hPE && lpNtHeaders);

	ntHdrs = GetNtHeaders32Address(hPE);

	if (ntHdrs->Signature == IMAGE_NT_SIGNATURE)
	{
		MemCopy(lpNtHeaders, ntHdrs, sizeof(IMAGE_NT_HEADERS32));
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------
mbool MPe32GetFileHeader(mhandle_t hPE, PIMAGE_FILE_HEADER lpFileHeader)
{
	PIMAGE_FILE_HEADER fileHeader = GetFileHeaderAddress(hPE);
	ASSERT_ARGS(hPE && lpFileHeader);
	MemCopy(lpFileHeader, fileHeader, sizeof(IMAGE_FILE_HEADER));
	return true;
}
//---------------------------------------------------------------------------------------
mbool MPe32GetOptionalHeader32(mhandle_t hPE, PIMAGE_OPTIONAL_HEADER32 lpOptionalHeader)
{
	PIMAGE_OPTIONAL_HEADER32 header = GetOptionalHeader32Address(hPE);
	ASSERT_ARGS(hPE && lpOptionalHeader);
	MemCopy(lpOptionalHeader, header, sizeof(IMAGE_OPTIONAL_HEADER32));
	return true;
}
//---------------------------------------------------------------------------------------
mbool MPe32GetDataDirectoryHeader(mhandle_t hPE, int index, PIMAGE_DATA_DIRECTORY lpDataDirectoryHeader)
{
	PIMAGE_DATA_DIRECTORY dataDir = GetDataDirectoryHeaderAddress(hPE, index);
	ASSERT_ARGS(hPE && lpDataDirectoryHeader && index > -1 && index < IMAGE_NUMBEROF_DIRECTORY_ENTRIES);
	MemCopy(lpDataDirectoryHeader, dataDir, sizeof(IMAGE_DATA_DIRECTORY));

	return true;
}
//---------------------------------------------------------------------------------------
mbool MPe32GetSectionHeader(mhandle_t hPE, int sectionIndex, PIMAGE_SECTION_HEADER lpSectionHeader)
{
	PIMAGE_SECTION_HEADER header;
	
	ASSERT_ARGS(hPE && sectionIndex > -1 && sectionIndex < (int)MPe32GetSectionCount(hPE) && lpSectionHeader);
	header = GetSectionHeaderAddress(hPE, sectionIndex);
	MemCopy(lpSectionHeader, header, sizeof(IMAGE_SECTION_HEADER));
	return true;
}
//---------------------------------------------------------------------------------------
unsigned int MPe32GetSectionCount(mhandle_t hPE)
{
	return GetNtHeaders32Address(hPE)->FileHeader.NumberOfSections;
}
//---------------------------------------------------------------------------------------
int MPe32FindSectionHeader(mhandle_t hPE, const char *lpszSectionName, PIMAGE_SECTION_HEADER lpSectionHeader)
{
	if (hPE && lpszSectionName)
	{
		unsigned int i;

		for (i = 0; i < MPe32GetSectionCount(hPE); i++)
		{
			IMAGE_SECTION_HEADER header;
			if (MPe32GetSectionHeader(hPE, i, &header) && (!strcmp(lpszSectionName, header.Name)))
			{
				if (lpSectionHeader)
					MemCopy(lpSectionHeader, &header, sizeof(IMAGE_SECTION_HEADER));

				return i;
			}
		}
	}

	return -1;
}
//---------------------------------------------------------------------------------------
mbool MPe32GetSectionData(mhandle_t hPE, const PIMAGE_SECTION_HEADER lpSectionHeader, void *lpSectionData, size_t nDataSize)
{
	ASSERT_ARGS(hPE && lpSectionHeader && nDataSize > 0 && lpSectionData);

	if (nDataSize > lpSectionHeader->SizeOfRawData)
		nDataSize = lpSectionHeader->SizeOfRawData;

	MemCopy(lpSectionData, GetSectionDataAddress(hPE, lpSectionHeader), nDataSize);
	return true;
}
//---------------------------------------------------------------------------------------
mbool MPe32SetDosStub(mhandle_t hPE, const void *lpBuffer, size_t nBufferSize)
{
	if (hPE && lpBuffer)
	{
		size_t realSize = MPe32GetDosStubSize(hPE);

		if (nBufferSize > realSize || !nBufferSize)
			nBufferSize = realSize;

		MemCopy(GetDosStubAddress(hPE), lpBuffer, nBufferSize);
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------
mbool MPe32SetDosHeader(mhandle_t hPE, const PIMAGE_DOS_HEADER lpDosHeader)
{
	IMPLEMENT_SET_HEADER(IMAGE_DOS_HEADER, hPE, GetDosHeaderAddress, lpDosHeader);
}
//---------------------------------------------------------------------------------------
mbool MPe32SetNtHeaders32(mhandle_t hPE, const PIMAGE_NT_HEADERS32 lpNtHeaders)
{
	IMPLEMENT_SET_HEADER(IMAGE_NT_HEADERS32, hPE, GetNtHeaders32Address, lpNtHeaders)
}
//---------------------------------------------------------------------------------------
mbool MPe32SetFileHeader(mhandle_t hPE, const PIMAGE_FILE_HEADER lpFileHeader)
{
	IMPLEMENT_SET_HEADER(IMAGE_FILE_HEADER, hPE, GetFileHeaderAddress, lpFileHeader)
}
//---------------------------------------------------------------------------------------
mbool MPe32SetOptionalHeader32(mhandle_t hPE, const PIMAGE_OPTIONAL_HEADER32 lpOptionalHeader)
{
	IMPLEMENT_SET_HEADER(IMAGE_OPTIONAL_HEADER32, hPE, GetOptionalHeader32Address, lpOptionalHeader)
}
//---------------------------------------------------------------------------------------
mbool MPe32SetSectionHeader(mhandle_t hPE, int sectionIndex, const PIMAGE_SECTION_HEADER lpSectionHeader)
{
	ASSERT_ARGS(hPE && sectionIndex > -1 && sectionIndex < (int)MPe32GetSectionCount(hPE) && lpSectionHeader);
	MemCopy(GetSectionHeaderAddress(hPE, sectionIndex), lpSectionHeader, sizeof(IMAGE_SECTION_HEADER));
	return true;
}
//---------------------------------------------------------------------------------------
mbool MPe32SetSectionData(mhandle_t hPE, const PIMAGE_SECTION_HEADER lpSectionHeader, const void *lpSectionData, size_t nDataSize)
{
	IMAGE_SECTION_HEADER header;
	ASSERT_ARGS(hPE && lpSectionHeader && lpSectionData && nDataSize > 0);
	
	if (MPe32FindSectionHeader(hPE, lpSectionHeader->Name, &header) != -1)
	{
		if (nDataSize > header.SizeOfRawData)
			nDataSize = header.SizeOfRawData;

		MemCopy(GetSectionDataAddress(hPE, lpSectionHeader), lpSectionData, nDataSize);
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------
mbool MPe32SetDataDirectoryHeader(mhandle_t hPE, int index, const PIMAGE_DATA_DIRECTORY lpDataDirectoryHeader)
{
	ASSERT_ARGS(hPE && index > -1 && index < IMAGE_NUMBEROF_DIRECTORY_ENTRIES && lpDataDirectoryHeader);
	MemCopy(GetDataDirectoryHeaderAddress(hPE, index), lpDataDirectoryHeader, sizeof(IMAGE_DATA_DIRECTORY));
	return true;
}
//---------------------------------------------------------------------------------------
mbool MPe32GetDataDirectoryData(mhandle_t hPE, int index, void *lpBuffer, size_t nBufferSize)
{
	PIMAGE_DATA_DIRECTORY directory = GetDataDirectoryHeaderAddress(hPE, index);
	ASSERT_ARGS(hPE && index > -1 && index < IMAGE_NUMBEROF_DIRECTORY_ENTRIES && lpBuffer && nBufferSize > 0);

	if (nBufferSize > directory->Size)
		nBufferSize = directory->Size;

	MemCopy(lpBuffer, GetDataDirectoryDataAddress(hPE, directory), nBufferSize);
	return true;
}
//---------------------------------------------------------------------------------------
static mbool InitNewSectionHeader(mhandle_t hPE, PIMAGE_SECTION_HEADER newHeader,
	const char *lpszSectionName, size_t nSectionSize, int lastHeaderIndex)
{
	PIMAGE_SECTION_HEADER lastHeader;

	lastHeader = GetSectionHeaderAddress(hPE, lastHeaderIndex);
	MemZero(newHeader, sizeof(IMAGE_SECTION_HEADER));

	newHeader->Characteristics = IMAGE_SCN_MEM_READ | IMAGE_SCN_MEM_WRITE | IMAGE_SCN_MEM_EXECUTE;
	newHeader->SizeOfRawData = MPE32_FILE_ALIGNMENT(hPE, nSectionSize);
	newHeader->PointerToRawData = MPE32_FILE_ALIGNMENT(hPE, lastHeader->PointerToRawData + lastHeader->SizeOfRawData);
	newHeader->Misc.VirtualSize = MPE32_SECTION_ALIGNMENT(hPE, newHeader->SizeOfRawData);
	newHeader->VirtualAddress = MPE32_SECTION_ALIGNMENT(hPE, lastHeader->VirtualAddress + lastHeader->Misc.VirtualSize);

	CWA(kernel32, lstrcpynA)(newHeader->Name, lpszSectionName, IMAGE_SIZEOF_SHORT_NAME);
	return true;
}
//---------------------------------------------------------------------------------------
int MPe32AddNewSection(mhandle_t hPE, const char *lpszSectionName, size_t nSectionSize)
{
	if (lpszSectionName && strlen(lpszSectionName) && nSectionSize)
	{
		EXPAND_PE_HANDLE(hPE);
		IMAGE_SECTION_HEADER newHeader;
		PIMAGE_SECTION_HEADER lpNewHeader;
		int lastIndex = MPe32GetSectionCount(hPE) - 1;

		if (InitNewSectionHeader(hPE, &newHeader, lpszSectionName, nSectionSize, lastIndex))
		{
			IMAGE_NT_HEADERS32 ntHeaders32;
			
			lpNewHeader = GetSectionHeaderAddress(hPE, lastIndex) + 1;
			MemCopy(lpNewHeader, &newHeader, sizeof(IMAGE_SECTION_HEADER));

			if (!MPe32GetNtHeaders32(hPE, &ntHeaders32))
				return -1;

			// Atualiza n�mero de se��es
			++ntHeaders32.FileHeader.NumberOfSections;
			// Atualiza SizeOfImage
			ntHeaders32.OptionalHeader.SizeOfImage = MPE32_SECTION_ALIGNMENT(GET_PE_HANDLE(), 
				ntHeaders32.OptionalHeader.SizeOfImage + sizeof(IMAGE_SECTION_HEADER) + newHeader.Misc.VirtualSize);
			// Atualiza SizeOfHeaders
			ntHeaders32.OptionalHeader.SizeOfHeaders = MPE32_FILE_ALIGNMENT(GET_PE_HANDLE(),
				ntHeaders32.OptionalHeader.SizeOfHeaders + sizeof(IMAGE_SECTION_HEADER)/* + newHeader.SizeOfRawData*/);

			if (!MPe32SetNtHeaders32(hPE, &ntHeaders32))
				return -1;

			GET_PE_MEMORY() = MemHGrow(GET_PE_MEMORY(), newHeader.SizeOfRawData);
			return GetNtHeaders32Address(hPE)->FileHeader.NumberOfSections;
		}
	}

	return -1;
}
//---------------------------------------------------------------------------------------
//static WORD GetImportHint(const char *lpszDllName, const char *lpszFuncName)
//{
//	HMODULE hDll = CWA(kernel32, LoadLibraryA)(lpszDllName);
//	if (hDll)
//	{
//		WORD wOrd = PEModGetExportOrdinal(hDll, lpszFuncName);
//		CWA(kernel32, FreeLibrary)(hDll);
//		return wOrd;
//	}
//
//	return 0;
//}
//---------------------------------------------------------------------------------------
static void SetupNewImportDir(mhandle_t hPE, void *pNewImportDir, unsigned int importCount, const char *lpszDllName, const char *lpszFuncName)
{
	PIMAGE_IMPORT_DESCRIPTOR pImportDescriptor;
	PIMAGE_THUNK_DATA32 pThunkData;
	PIMAGE_IMPORT_BY_NAME pImportByName;
	mbyte *pImportData;
	char *pszDllName;

	// Seta refer�ncia de IMAGE_IMPORT_DESCRIPTOR
	pImportDescriptor = (PIMAGE_IMPORT_DESCRIPTOR)pNewImportDir;
	pImportDescriptor += importCount;

	pImportData = (mbyte *)(pImportDescriptor + 2);

	// Seta refer�ncia de IMAGE_THUNK_DATA32
	pThunkData = (PIMAGE_THUNK_DATA32)pImportData;
	pImportData += sizeof(IMAGE_THUNK_DATA32) * 2;

	// Copia o nome da DLL
	CWA(kernel32, lstrcpyA)((char *)pImportData, lpszDllName);
	pszDllName = (char *)pImportData;
	pImportData += strlen(pszDllName) + 1;

	// Seta refer�ncia de IMAGE_IMPORT_BY_NAME
	pImportByName = (PIMAGE_IMPORT_BY_NAME)pImportData;
	pImportByName->Hint = 0;//GetImportHint(lpszDllName, lpszFuncName);
	CWA(kernel32, lstrcpyA)((char *)pImportByName->Name, lpszFuncName);

	pImportDescriptor->OriginalFirstThunk = VAToRVA(hPE, pThunkData);
	pImportDescriptor->FirstThunk = pImportDescriptor->OriginalFirstThunk;
	pImportDescriptor->ForwarderChain = 0;
	pImportDescriptor->Name = VAToRVA(hPE, pszDllName);
	pImportDescriptor->TimeDateStamp = 0;
	
	pThunkData->u1.AddressOfData = VAToRVA(hPE, pImportByName);
}

static void UpdateImageSizeAfterImportAdded(mhandle_t hPE, PIMAGE_SECTION_HEADER section, size_t virtualSizeAdded, size_t fileSizeAdded)
{
	PIMAGE_NT_HEADERS32 ntHeaders;

	// Atualiza tamanho da se��o.
	section->SizeOfRawData = MPE32_FILE_ALIGNMENT(hPE, section->SizeOfRawData + fileSizeAdded);
	section->Misc.VirtualSize = MPE32_SECTION_ALIGNMENT(hPE, section->Misc.VirtualSize + virtualSizeAdded);

	// Atualiza SizeOfImage
	ntHeaders = GetNtHeaders32Address(hPE);
	ntHeaders->OptionalHeader.SizeOfImage = MPE32_SECTION_ALIGNMENT(hPE, ntHeaders->OptionalHeader.SizeOfImage + virtualSizeAdded);
}

static mbool ExpandImageToHoldNewImports(mhandle_t hPE, size_t oldIATSize, const char *lpszDllName, const char *lpszFuncName, 
	unsigned int importCount, size_t *pFileSizeAdded, size_t *pVirtualSizeAdded, /*unsigned long *pOldFileEndOffset,*/ unsigned long *pNewFileEndOffset)
{
	size_t newIATSize = oldIATSize + sizeof(IMAGE_IMPORT_DESCRIPTOR) + sizeof(IMAGE_THUNK_DATA32) * 2 + 
		sizeof(IMAGE_IMPORT_BY_NAME) + strlen(lpszDllName) + strlen(lpszFuncName) + 3;

	EXPAND_PE_HANDLE(hPE);

	//*pOldFileEndOffset = MPe32MemorySize(hPE);
	*pFileSizeAdded = MPE32_FILE_ALIGNMENT(hPE, newIATSize);
	*pVirtualSizeAdded = MPE32_SECTION_ALIGNMENT(hPE, newIATSize);

	// Expande da mem�ria para copiar dados do diret�rio de importa��o.
	if (!MGrowPEMemory(GET_PE_HANDLE(), *pFileSizeAdded))
		return false;

	*pNewFileEndOffset = MPe32MemorySize(hPE);

	return true;
}

static void *MoveImportTable(mhandle_t hPE, size_t oldIATSize, unsigned long newFileEndOffset)
{
	EXPAND_PE_HANDLE(hPE);

	// Pegamos os dados do diret�rio de importa��o antigo.
	void *pOldImportDir = GetImageImportDescriptorAddress(hPE);
	// Pega header da �ltima se��o.
	PIMAGE_SECTION_HEADER section = GetSectionHeaderAddress(hPE, MPe32GetSectionCount(hPE) - 1);
	// Pega endere�o alocado para novo diret�rio de importa��o.
	unsigned long newImportDirOffset = section->PointerToRawData + section->SizeOfRawData;
	void *pNewImportDir = &((mbyte *)GET_PE_MEMORY())[newImportDirOffset];

	MemZero(pNewImportDir, newFileEndOffset - newImportDirOffset);
	// Copia dados do antigo diret�rio para o novo.
	MemCopy(pNewImportDir, pOldImportDir, oldIATSize);

	return pNewImportDir;
}

//static ExpandImportSection(mhandle_t hPE, PIMAGE_SECTION_HEADER)

mbool MPe32AddImport(mhandle_t hPE, const char *lpszDllName, const char *lpszFuncName)
{
	PIMAGE_SECTION_HEADER section;
	PIMAGE_DATA_DIRECTORY directory;
	size_t fileSizeAdded, virtualSizeAdded, oldIATSize;
	unsigned long importCount, newFileEndOffset;
	void *pNewImportDir;

	ASSERT_ARGS(hPE && strlen(lpszDllName) && strlen(lpszFuncName));

	importCount = MPe32WalkImportDescriptor(hPE, NULL, NULL);
	oldIATSize = importCount * sizeof(IMAGE_IMPORT_DESCRIPTOR);

	if (ExpandImageToHoldNewImports(hPE, oldIATSize, lpszDllName, lpszFuncName, importCount, &fileSizeAdded, 
			&virtualSizeAdded, &newFileEndOffset))
	{
		section = GetSectionHeaderAddress(hPE, MPe32GetSectionCount(hPE) - 1);
		pNewImportDir = MoveImportTable(hPE, oldIATSize, newFileEndOffset);

		// Faz refer�ncia para novo diret�rio de importa��o.
		directory = GetDataDirectoryHeaderAddress(hPE, IMAGE_DIRECTORY_ENTRY_IMPORT);
		directory->VirtualAddress = section->VirtualAddress + section->SizeOfRawData;

		UpdateImageSizeAfterImportAdded(hPE, section, virtualSizeAdded, fileSizeAdded);
		SetupNewImportDir(hPE, pNewImportDir, importCount, lpszDllName, lpszFuncName);

		return true;
	}

	return false;
}
//---------------------------------------------------------------------------------------
unsigned int MPe32WalkImportDescriptor(mhandle_t hPE, MPe32WalkImportDescriptorProc callback, void *pUserData)
{
	register unsigned int result = 0;
	PIMAGE_IMPORT_DESCRIPTOR descriptor = GetImageImportDescriptorAddress(hPE);

	while (descriptor->Name)
	{
		if (callback)
		{
			const char *name = RVAToVA(hPE, descriptor->Name);
			callback(name, descriptor, pUserData);
		}

		++descriptor;
		++result;
	}

	return result;
}
//---------------------------------------------------------------------------------------
unsigned int MPe32WalkImageThunkData(mhandle_t hPE, const char *lpszDllName, MPe32WalkImageThunkDataProc callback, void *pUserData)
{
	register unsigned int result = 0;
	PIMAGE_IMPORT_DESCRIPTOR descriptor = GetImageImportDescriptorAddress(hPE);

	ASSERT_ARGS(hPE && lpszDllName && callback && pUserData);

	while (descriptor->Name)
	{
		const char *dllName = RVAToVA(hPE, descriptor->Name);

		if (CWA(kernel32, lstrcmpiA)(dllName, lpszDllName) == 0)
		{
			PIMAGE_THUNK_DATA32 thunk = descriptor->OriginalFirstThunk != 0 ? 
				(PIMAGE_THUNK_DATA32)RVAToVA(hPE, descriptor->OriginalFirstThunk) : (PIMAGE_THUNK_DATA32)RVAToVA(hPE, descriptor->FirstThunk);
			
			while (thunk->u1.AddressOfData)
			{
				const char *pszFuncName = NULL;

				if (!(thunk->u1.Ordinal & IMAGE_ORDINAL_FLAG32))
				{
					PIMAGE_IMPORT_BY_NAME name = (PIMAGE_IMPORT_BY_NAME)RVAToVA(hPE, thunk->u1.AddressOfData);
					pszFuncName = name->Name;
				}

				if (callback)
					callback(pszFuncName, thunk, pUserData);

				++result;
				++thunk;
			}

			break;
		}

		++descriptor;
	}

	return result;
}