#ifndef __PE32_H__
#define	__PE32_H__

#include "common.h"
#include "coredefs.h"

EXPORT_AS_C()

typedef enum {omCreate = 0, omOpen} MPeOpenMode;

// Retorna NULL se falhar.
mhandle_t MPe32Open(const mchar *lpFileName, MPeOpenMode mode);

#define	MPe32OpenFile(lpFileName)			MPe32Open(lpFileName, omOpen)
#define	MPe32CreateEmptyFile(lpFileName)	MPe32Open(lpFileName, omCreate)

mbool MPe32SaveFile(mhandle_t hPE, const mchar *lpFileName);
mbool MPe32Close(mhandle_t);

unsigned int MPe32GetDosStubSize(mhandle_t hPE);
mbool MPe32GetDosStub(mhandle_t hPE, void *lpBuffer, size_t nBufferSize);
mbool MPe32GetDosHeader(mhandle_t hPE, PIMAGE_DOS_HEADER lpDosHeader);
mbool MPe32GetNtHeaders32(mhandle_t hPE, PIMAGE_NT_HEADERS32 lpNtHeaders);
mbool MPe32GetFileHeader(mhandle_t hPE, PIMAGE_FILE_HEADER lpFileHeader);
mbool MPe32GetOptionalHeader32(mhandle_t hPE, PIMAGE_OPTIONAL_HEADER lpOptionalHeader);
mbool MPe32GetDataDirectoryHeader(mhandle_t hPE, int index, PIMAGE_DATA_DIRECTORY lpDataDirectoryHeader);
mbool MPe32GetDataDirectoryData(mhandle_t hPE, int index, void *lpBuffer, size_t nBufferSize);

// Fun��es para se��es.
unsigned int MPe32GetSectionCount(mhandle_t hPE);
mbool MPe32GetSectionHeader(mhandle_t hPE, int sectionIndex, PIMAGE_SECTION_HEADER lpSectionHeader);

// Fun��es para a tabela de importa��es (IAT)
typedef void (__stdcall *MPe32WalkImportDescriptorProc)(const char *lpszDllName, const PIMAGE_IMPORT_DESCRIPTOR descriptor, void *pUserData);
typedef void (__stdcall *MPe32WalkImageThunkDataProc)(const char *lpszFuncName, const PIMAGE_THUNK_DATA32 thunk, void *pUserData);

unsigned int MPe32WalkImportDescriptor(mhandle_t hPE, MPe32WalkImportDescriptorProc callback, void *pUserData);
unsigned int MPe32WalkImageThunkData(mhandle_t hPE, const char *lpszDllName, MPe32WalkImageThunkDataProc callback, void *pUserData);

// Retorna index da se��o ou -1 caso se��o n�o exista.
int MPe32FindSectionHeader(mhandle_t hPE, const char *lpszSectionName, PIMAGE_SECTION_HEADER lpSectionHeader);
mbool MPe32GetSectionData(mhandle_t hPE, const PIMAGE_SECTION_HEADER lpSectionHeader, void *lpSectionData, size_t nDataSize);

mbool MPe32SetDosStub(mhandle_t hPE, const void *lpBuffer, size_t nBufferSize);
mbool MPe32SetDosHeader(mhandle_t hPE, const PIMAGE_DOS_HEADER lpDosHeader);
mbool MPe32SetNtHeaders32(mhandle_t hPE, const PIMAGE_NT_HEADERS32 lpNtHeaders);
mbool MPe32SetFileHeader(mhandle_t hPE, const PIMAGE_FILE_HEADER lpFileHeader);
mbool MPe32SetOptionalHeader32(mhandle_t hPE, const PIMAGE_OPTIONAL_HEADER32 lpOptionalHeader);
mbool MPe32SetDataDirectoryHeader(mhandle_t hPE, int index, const PIMAGE_DATA_DIRECTORY lpDataDirectoryHeader);

mbool MPe32SetSectionHeader(mhandle_t hPE, int sectionIndex, const PIMAGE_SECTION_HEADER lpSectionHeader);
mbool MPe32SetSectionData(mhandle_t hPE, const PIMAGE_SECTION_HEADER lpSectionHeader, const void *lpSectionData, size_t nDataSize);

int MPe32AddNewSection(mhandle_t hPE, const char *lpszSectionName, size_t nSectionSize);
mbool MPe32AddImport(mhandle_t hPE, const char *lpszDllName, const char *lpszFuncName);

EXPORT_AS_C_END()

#endif // __PE32_H__