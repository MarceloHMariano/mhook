#include "PEDescriptor.h"
#include "Memory.h"
#include "Utils/FileUtils.h"
#include "PE32.h"

MPe32Descriptor *MNewPE32Descriptor(HANDLE fileHandle)
{
	IMAGE_DOS_HEADER dosHeader;
	MemZero(&dosHeader, sizeof(IMAGE_DOS_HEADER));

	MReadFile(fileHandle, &dosHeader, sizeof(IMAGE_DOS_HEADER), 0, FILE_BEGIN);
	if (dosHeader.e_magic == IMAGE_DOS_SIGNATURE)
	{
		IMAGE_NT_HEADERS32 ntHeaders32;

		MReadFile(fileHandle, &ntHeaders32, sizeof(IMAGE_NT_HEADERS32), dosHeader.e_lfanew, FILE_BEGIN);
		if (ntHeaders32.Signature == IMAGE_NT_SIGNATURE)
		{
			DWORD dwFileSize = CWA(kernel32, GetFileSize)(fileHandle, NULL);

			if (dwFileSize > 0)
			{
				MPe32Descriptor *descriptor = (MPe32Descriptor *)MemHAlloc(sizeof(MPe32Descriptor));

				if (descriptor)
				{
					//descriptor->FileHandle = fileHandle;
					descriptor->BaseAddress = MemVAlloc(NULL, dwFileSize, PAGE_READWRITE); // Aloca��o virtual para arquivos.

					if (descriptor->BaseAddress && MReadFileAll(fileHandle, descriptor->BaseAddress, dwFileSize, 0, FILE_BEGIN))
						return descriptor;
				}
			}
		}
	}
	
	return NULL;
}

void MFreePE32Descriptor(MPe32Descriptor *descriptor)
{
	MemVFree(descriptor->BaseAddress);
	MemHFree(descriptor);
}

mbool MGrowPEMemory(MPe32Descriptor *descriptor, size_t newSize)
{
	void *p = descriptor->BaseAddress;
	descriptor->BaseAddress = MemVRealloc(p, newSize, PAGE_READWRITE);

	return descriptor->BaseAddress != NULL;
}