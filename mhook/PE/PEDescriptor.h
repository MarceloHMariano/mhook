#ifndef __PE_DESCRIPTOR_H__
#define	__PE_DESCRIPTOR_H__

#include "common.h"

typedef struct _MPe32Descriptor
{
	//mhandle_t FileHandle;
	mhandle_t BaseAddress;
} MPe32Descriptor;

MPe32Descriptor *MNewPE32Descriptor(HANDLE fileHandle);
void MFreePE32Descriptor(MPe32Descriptor *descriptor);
mbool MGrowPEMemory(MPe32Descriptor *descriptor, size_t newSize);

#define	MPe32MemorySize(mhandle_t)	MemVSize(((MPe32Descriptor *)mhandle_t)->BaseAddress)

#endif // __PE_DESCRIPTOR_H__