#include "..\PE.h"

static int _strlen(const char *s)
{
	int i = 0;
	while (*s)
	{
		i++;
		s++;
	}
	return i;
}

static int _strcmp(const char *s1, const char *s2)
{
	int len1 = _strlen((char *)s1);
	int len2 = _strlen((char *)s2);

	if (len1 != len2)
		return len1 > len2 ? -1 : 1;

	while (*s1 && *s2)
	{
		char up1 = *s1, up2 = *s2;

		if (up1 >= 0x61)
			up1 -= 0x20; // toupper(up1)

		if (up2 >= 0x61)
			up2 -= 0x20; // toupper(up2)

		if (up1 != up2)
			return up1 > up2 ? -1 : 1;

		s1++;
		s2++;
	}

	return 0;
}

PIMAGE_DOS_HEADER PEModGetDOSHeader(HMODULE hMod)
{
    PIMAGE_DOS_HEADER lpResult = (PIMAGE_DOS_HEADER)hMod;

	// Checa assinatura do cabe�alho antes de retornar.
    return (lpResult->e_magic == IMAGE_DOS_SIGNATURE) ? lpResult : NULL;
}

PIMAGE_NT_HEADERS PEModGetNtHeaders(HMODULE hMod)
{
    PIMAGE_NT_HEADERS lpResult = (PIMAGE_NT_HEADERS)PEModGetDOSHeader(hMod);
    if (lpResult)
    {
        lpResult = (PIMAGE_NT_HEADERS)((DWORD)lpResult + ((PIMAGE_DOS_HEADER)lpResult)->e_lfanew);

		// Checa assinatura do cabe�alho antes de retornar.
        return lpResult->Signature == IMAGE_NT_SIGNATURE ? lpResult : NULL;
    }

    return NULL;
}

PIMAGE_FILE_HEADER PEModGetFileHeader(HMODULE hMod)
{
    PIMAGE_FILE_HEADER lpResult = (PIMAGE_FILE_HEADER)PEModGetNtHeaders(hMod);
    if (lpResult)
    {
        lpResult = &((PIMAGE_NT_HEADERS)lpResult)->FileHeader;
        return lpResult;
    }

    return NULL;
}

PIMAGE_OPTIONAL_HEADER PEModGetOptionalHeader(HMODULE hMod)
{
    PIMAGE_OPTIONAL_HEADER lpResult = (PIMAGE_OPTIONAL_HEADER)PEModGetNtHeaders(hMod);
    if (lpResult)
    {
        lpResult = &((PIMAGE_NT_HEADERS)lpResult)->OptionalHeader;

		// Checa assinatura do cabe�alho antes de retornar.
        return lpResult->Magic == IMAGE_NT_OPTIONAL_HDR_MAGIC ? lpResult : NULL;
    }

    return NULL;
}

PIMAGE_DATA_DIRECTORY PEModGetImageDataDirectory(HMODULE hModule, BYTE nDirectryIndex)
{
    PIMAGE_OPTIONAL_HEADER lpOptionalHdr = PEModGetOptionalHeader(hModule);
    if (lpOptionalHdr)
        return &lpOptionalHdr->DataDirectory[nDirectryIndex];

    return NULL;
}

static WORD GetExportOrdinal(HMODULE hModule, PIMAGE_EXPORT_DIRECTORY lpExportDir, const char *lpProcName)
{
	register DWORD i;

	register char **lpNames = (char **)((DWORD)hModule + lpExportDir->AddressOfNames);
	register PWORD lpwOrds = (PWORD)((DWORD)hModule + lpExportDir->AddressOfNameOrdinals);

	for (i = 0; i < lpExportDir->NumberOfNames; i++)
		if (_strcmp((char *)((DWORD)hModule + lpNames[i]), lpProcName) == 0)
			return lpwOrds[i];

	return 0;
}

WORD PEModGetExportOrdinal(HMODULE hModule, const char *lpProcName)
{
	PIMAGE_EXPORT_DIRECTORY lpExportDir = PEModGetModuleExportDirectory(hModule);
	return GetExportOrdinal(hModule, lpExportDir, lpProcName);
}

//PDWORD __PEModGetExportAddress(HMODULE hModule, const char *lpProcName)
//{
//	for (i = 0; i < lpExportDir->NumberOfNames; i++)
//	{
//		if (_strcmp((char *)((DWORD)hModule + lpNames[i]), lpProcName) == 0)
//		{
//			WORD wOrd = lpwOrds[i];
//			PDWORD lpdwAddress = (PDWORD)((DWORD)hModule + lpExportDir->AddressOfFunctions);
//			return &lpdwAddress[wOrd];
//		}
//	}
//
//	return NULL;
//}

FARPROC WINAPI PEModGetExportAddress(HMODULE hModule, const char *lpProcName)
{
	PIMAGE_EXPORT_DIRECTORY lpExportDir = PEModGetModuleExportDirectory(hModule);
	WORD wOrdinal = GetExportOrdinal(hModule, lpExportDir, lpProcName);
	PDWORD pdwExportAddrTable = (PDWORD)((DWORD)hModule + lpExportDir->AddressOfFunctions);
	
	FARPROC pExportAddr = (FARPROC)((DWORD)hModule + (DWORD)pdwExportAddrTable[wOrdinal]);
	return pExportAddr;
}