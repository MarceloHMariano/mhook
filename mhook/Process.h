/*============================================================================================
 *
 *	Arquivo:	Process.h
 *	Autor:		Marcelo Henrique
 *
 *	Descri��o:
 *
 *		Este arquivo cont�m fun��es que encapsulam rotinas usadas para manipula��o de
 *		processos do Windows.
 *
 *============================================================================================
 */

#ifndef __PROCESS_UTLS_H__
#define __PROCESS_UTLS_H__

#include "PE.h"
#include "Process/ProcessGlobal.h"

EXPORT_AS_C()

#define	PS_PROCESS_READ_FLAGS	PROCESS_VM_READ
#define	PS_PROCESS_WRITE_FLAGS	PROCESS_VM_WRITE | PROCESS_VM_OPERATION
#define	PS_CREATE_THREAD_FLAGS	PS_PROCESS_WRITE_FLAGS | PROCESS_CREATE_THREAD | PROCESS_QUERY_INFORMATION \
								| PROCESS_VM_OPERATION | PROCESS_VM_READ 

/////////////////////////////////////////////////////////////////////////////
// Fun��es para enumera��o de processos
/////////////////////////////////////////////////////////////////////////////

typedef BOOL (CALLBACK *PRSS_ENUM_PROC)(void *lpEnumStruct, void *lpParam);

BOOL PsEnumProcesses(PRSS_ENUM_PROC lpfnCallback, void *lpParam);
BOOL PsEnumProcessThreads(DWORD dwPID, PRSS_ENUM_PROC lpfnCallback, void *lpParam);
BOOL PsEnumProcessModules(DWORD dwPID, PRSS_ENUM_PROC lpfnCallback, void *lpParam);

/////////////////////////////////////////////////////////////////////////////
// Fun��es para m�dulos de processos
/////////////////////////////////////////////////////////////////////////////

/* Retorna quantidade de m�dulos carregados pelo processo associado ao PID dwPID. */
DWORD PsGetProcessModuleCount(DWORD dwPID);

/* Retorna quantidade de m�dulos carregados pelo processo atual. */
DWORD PsGetCurrentProcessModuleCount();

/* Retorna handle do m�dulo solicitado em lpModuleName */
HMODULE WINAPI PsGetModuleBaseAddressA(const char *lpModuleName);
HMODULE WINAPI PsGetModuleBaseAddressW(const wchar_t *lpModuleName);

#define PsGetMainModuleBaseAddress() PsGetModuleBaseAddressW(NULL)

/* Retorna handle de m�dulo principal de um processo remoto */
HMODULE PsGetRemoteProcessModuleHandle(DWORD dwPID);

HMODULE PsGetKernel32BaseAddress();

/////////////////////////////////////////////////////////////////////////////
// Fun��es para inje��o de c�digo
/////////////////////////////////////////////////////////////////////////////

/* Injeta m�dulo especificado em lpModuleName no processo de handle especificado em hProcess. */
HANDLE PsInjectModule(DWORD dwPID, const char *lpModuleName);

// Cria um projeto com c�digo de outro execut�vel injetado nele
HANDLE PsCreateHollowProcess(char *lpszOriginalProcess, LPCTSTR lpszReplacementFile);

// Retorna quantidades de bytes escritos na mem�ria de um processo remoto
DWORD PsWriteMemory(DWORD dwPID, void *lpAddr, const void *lpBuffer, DWORD dwSize);
// Retorna quantidades de bytes lidos na mem�ria de um processo remoto
DWORD PsReadMemory(DWORD dwPID, void *lpAddr, void *lpBuffer, DWORD dwSize);
// Retorna quantidades de bytes escritos na mem�ria de um processo remoto
DWORD PsWriteHandleMemory(HANDLE hProcess, void *lpAddr, const void *lpBuffer, DWORD dwSize);
// Retorna quantidades de bytes lidos na mem�ria de um processo remoto
DWORD PsReadHandleMemory(HANDLE hProcess, void *lpAddr, void *lpBuffer, DWORD dwSize);

/////////////////////////////////////////////////////////////////////////////
// Fun��es utilit�rias
/////////////////////////////////////////////////////////////////////////////

/* Procura pelo processo especificado por lpImageName e retorna seu PID, caso o encontre.
   Caso contr�rio, retorna zero (0).
*/
DWORD PsGetPidByImage(LPCTSTR lpImageName);

EXPORT_AS_C_END()

#endif // __PROCESS_UTLS_H__
