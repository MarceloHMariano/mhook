#include "../Process.h"
#include "Enumeration.h"

static BOOL PsEnum(PENUM_CALLBACK_PROC_DATA lpCallbackData, PTLHELP32_SNAPSHOT_DATA lpSnapshotData)
{
	if (lpCallbackData->lpfnCallback)
	{
		HANDLE hSnapshot = CWA(kernel32, CreateToolhelp32Snapshot)(lpSnapshotData->dwSnapFlag, lpSnapshotData->dwProcessId);

		if (hSnapshot != INVALID_HANDLE_VALUE)
		{
			if (lpSnapshotData->lpfnSnapFirst(hSnapshot, lpSnapshotData->lpSnapStruct))
			{
				BOOL bEnum = lpCallbackData->lpfnCallback(lpSnapshotData->lpSnapStruct, lpCallbackData->lpParam);

				if (bEnum)
				{
					while (bEnum && lpSnapshotData->lpfnSnapNext(hSnapshot, lpSnapshotData->lpSnapStruct))
						bEnum = lpCallbackData->lpfnCallback(lpSnapshotData->lpSnapStruct, lpCallbackData->lpParam);
				}
			}

			CWA(kernel32, CloseHandle)(hSnapshot);
			return TRUE;
		}
	}
	return FALSE;
}

#define	EXEC_PROCESS_ENUMERATION(lpfnCallback, \
	lpCallbackParam, \
	TlHelpSnapFirstFunc, \
	TlHelpSnapNextFunc,	\
	TlHelpSnapStruct, \
	TlHelpSnapFlag, \
	TlHelpSnapPid) \
{\
	TlHelpSnapStruct snapStruct; \
	ENUM_CALLBACK_PROC_DATA callbackData; \
	TLHELP32_SNAPSHOT_DATA snapData; \
	\
	snapStruct.dwSize = sizeof(TlHelpSnapStruct); \
	\
	snapData.lpfnSnapFirst = (TLHELP32_SNAPSHOT_PROC)PEModGetExportAddress(PsGetKernel32BaseAddress(), GWA(TlHelpSnapFirstFunc)); \
	snapData.lpfnSnapNext = (TLHELP32_SNAPSHOT_PROC)PEModGetExportAddress(PsGetKernel32BaseAddress(), GWA(TlHelpSnapNextFunc)); \
	snapData.lpSnapStruct = &snapStruct; \
	snapData.dwSnapFlag = TlHelpSnapFlag; \
    snapData.dwProcessId = TlHelpSnapPid; \
	\
	callbackData.lpfnCallback = lpfnCallback; \
	callbackData.lpParam = lpCallbackParam; \
	PsEnum(&callbackData, &snapData); \
}

BOOL PsEnumProcesses(PRSS_ENUM_PROC lpfnCallback, void *lpParam)
	EXEC_PROCESS_ENUMERATION(lpfnCallback, lpParam, Process32First,
		Process32Next, PROCESSENTRY32, TH32CS_SNAPPROCESS, 0)

BOOL PsEnumProcessThreads(DWORD dwPID, PRSS_ENUM_PROC lpfnCallback, void *lpParam)
	EXEC_PROCESS_ENUMERATION(lpfnCallback, lpParam, Thread32First,
		Thread32Next, THREADENTRY32, TH32CS_SNAPTHREAD, dwPID)

BOOL PsEnumProcessModules(DWORD dwPID, PRSS_ENUM_PROC lpfnCallback, void *lpParam)
	EXEC_PROCESS_ENUMERATION(lpfnCallback, lpParam, Module32First,
		Module32Next, MODULEENTRY32, TH32CS_SNAPMODULE, dwPID)
