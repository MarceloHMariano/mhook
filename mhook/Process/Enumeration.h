#ifndef __ENUMERATION_H__
#define	__ENUMERATION_H__

#include "../Process.h"

typedef BOOL (WINAPI *TLHELP32_SNAPSHOT_PROC)(HANDLE, void *);

typedef struct _ENUM_CALLBACK_PROC_DATA
{
	PRSS_ENUM_PROC lpfnCallback;
	void *lpParam;
} ENUM_CALLBACK_PROC_DATA, *PENUM_CALLBACK_PROC_DATA;

typedef struct _TLHELP32_SNAPSHOT_DATA
{
	TLHELP32_SNAPSHOT_PROC lpfnSnapFirst;
	TLHELP32_SNAPSHOT_PROC lpfnSnapNext;
	void *lpSnapStruct;
	DWORD dwSnapFlag;
	DWORD dwProcessId;

} TLHELP32_SNAPSHOT_DATA, *PTLHELP32_SNAPSHOT_DATA;

#endif // __ENUMERATION_H__