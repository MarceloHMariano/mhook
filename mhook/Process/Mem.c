#include "..\Process.h"

// Para opera��es de mem�ria em processo
typedef enum
{
	PROCESS_MEM_READ = 0x1,
	PROCESS_MEM_WRITE
} ProcessOperationType;

static DWORD ProcessMemOperation(DWORD dwPID, void *lpAddr, void *lpBuffer, DWORD dwSize, ProcessOperationType operation)
{
	DWORD dwFlags = operation == PROCESS_MEM_READ ? PS_PROCESS_READ_FLAGS : PS_PROCESS_WRITE_FLAGS;
	HANDLE hProcess = CWA(kernel32, OpenProcess)(dwFlags, FALSE, dwPID);

	if (hProcess)
	{
		DWORD dwRet = 0;
		BOOL bOp;

		if (operation == PROCESS_MEM_READ)
			bOp = CWA(kernel32, ReadProcessMemory)(hProcess, lpAddr, lpBuffer, dwSize, &dwRet);
		else
		{
			DWORD dwOldProtect;

			if (CWA(kernel32, VirtualProtectEx)(lpAddr, lpAddr, dwSize, PAGE_EXECUTE_READWRITE, &dwOldProtect))
			{
				bOp = CWA(kernel32, WriteProcessMemory)(hProcess, lpAddr, lpBuffer, dwSize, &dwRet);
				CWA(kernel32, VirtualProtect)(lpAddr, dwSize, dwOldProtect, &dwOldProtect);
			}
		}

		CWA(kernel32, CloseHandle)(hProcess);

		if (bOp)
			return dwRet;
	}

	return 0;
}

DWORD PsWriteMemory(DWORD dwPID, void *lpAddr, const void *lpBuffer, DWORD dwSize)
{
	return ProcessMemOperation(dwPID, lpAddr, (void *)lpBuffer, dwSize, PROCESS_MEM_WRITE);
}

DWORD PsReadMemory(DWORD dwPID, void *lpAddr, void *lpBuffer, DWORD dwSize)
{
	return ProcessMemOperation(dwPID, lpAddr, lpBuffer, dwSize, PROCESS_MEM_READ);
}

DWORD PsWriteHandleMemory(HANDLE hProcess, void *lpAddr, const void *lpBuffer, DWORD dwSize)
{
	DWORD dwBytes = 0;

	if (CWA(kernel32, WriteProcessMemory)(hProcess, lpAddr, lpBuffer, dwSize, &dwBytes))
		return dwBytes;
	else
		return 0;
}

DWORD PsReadHandleMemory(HANDLE hProcess, void *lpAddr, void *lpBuffer, DWORD dwSize)
{
	DWORD dwBytes = 0;
	
	if (CWA(kernel32, ReadProcessMemory)(hProcess, lpAddr, lpBuffer, dwSize, &dwBytes))
		return dwBytes;
	else
		return 0;
}