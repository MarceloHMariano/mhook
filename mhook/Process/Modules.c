#include "..\Process.h"

HMODULE WINAPI PsGetModuleBaseAddressA(const char *lpModuleName)
{
	ANSI_STRING szInDllName;
	UNICODE_STRING szOutDllName;
	HMODULE hModule;

	CWA(ntdll, RtlInitAnsiString)(&szInDllName, lpModuleName);
	CWA(ntdll, RtlAnsiStringToUnicodeString)(&szOutDllName, &szInDllName, TRUE);

	hModule = PsGetModuleBaseAddressW(szOutDllName.Buffer);

	CWA(ntdll, RtlFreeUnicodeString)(&szOutDllName);

	return hModule;
}

static NAKED unsigned int GetTextHash(const wchar_t *lpModuleName)
{
	__asm
	{
		push edx
		push edi
		cld							// Limpa direction flag

		xor eax, eax
		mov edi, esi
	scan_str:
		lodsw						// while (*esi != '\0') esi++;
		test eax, eax
		jne scan_str

		sub esi, 0x4
		mov ecx, esi				// ecx aponta para o �ltimo caracter da string

		mov dl, 0x2E				// P�e '.' em DL
		std							// seta direction flag
	scan_dot:
		lodsw
		cmp byte ptr [esi], dl		// V� se pegamos o '.'
		je found_dot
		test edi, esi
		jne scan_dot
		jmp end_scan_dot

	found_dot:
		mov ecx, esi
		mov esi, edi

	end_scan_dot:
		cld
		xor edi, edi				// limpa edi que vai guardar o hash do nome da dll

	loop_mod_name:
		xor eax, eax
		lodsw
		test eax, eax
		je get_hash_return
		cmp al, 0x61				// compara com 'a' para sabermos se o nome est� em lowercase
		jl not_lower
		sub al, 0x20				// Subtrai 32 de AL. Isso � o que toupper() faz.

	not_lower:
		ror edi, 22					// rotate com minha idade
		add edi, eax
		cmp ecx, esi
		je get_hash_return
		jmp loop_mod_name

	get_hash_return:
		mov eax, edi				// return edi
		pop edi
		pop edx
		ret
	}
}

NAKED HMODULE WINAPI PsGetModuleBaseAddressW(const wchar_t *lpModuleName)
{
#if defined(MSVC) || defined(BORLAND_C)
	__asm
	{
		push ebp
		mov ebp, esp

		cmp [ebp + 8], 0
		jne begin_mod
		mov eax, fs:[0x18]		// teb
		mov eax, [eax + 0x30]	// teb->peb
		mov eax, [eax + 0x8]	// peb->ImageBaseAddress
		jmp mod_ret

	begin_mod:
		push ebx
		push esi
		push edi
		xor edx, edx

		mov esi, [ebp+8]
		call GetTextHash			// get_hash(lpModuleName)
		mov edi, eax

		mov edx, fs:[edx + 0x30]	// peb
		mov edx, [edx + 0xC]		// peb->LoaderData
		mov edx, [edx + 0xC]		// (PLDR_DATA_TABLE_ENTRY)ldrData->InLoadOrderModuleList.FLink

	next_mod:
		test edx, edx
		je not_found_mod

		mov esi, [edx + 0x30]		// ldrDataTableEntry->BaseDllName.Buffer
		call GetTextHash			// get_hash(esi)

		cmp edi, eax				// compara o hash de lpModuleName com o hash salvo em eax
		mov ebx, [edx + 0x18]		// ldrDataTableEntry->DllBase
		mov edx, [edx]				// (PLDR_DATA_TABLE_ENTRY)ldrDataTableEntry->InLoadOrderLinks.FLink
		jne next_mod
		mov eax, ebx
		jmp end_mod

	not_found_mod:
		xor eax, eax

	end_mod:
		pop edi
		pop esi
		pop ebx
	mod_ret:
		leave
		ret 0x4
	}
#elif defined(GCC)
	return GetModuleHandleW(lpModuleName);
#else
#error "Imposs�vel compilar PsGetModuleBaseAddressW sem detectar o compilador usado."
#endif
}

DWORD PsGetProcessModuleCount(DWORD dwPID)
{
    // Tentamos abrir o processo. Se falhar aqui, n�o teremos como prosseguir!
    HANDLE hProcess = CWA(kernel32, OpenProcess)(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, dwPID);
    if (hProcess)
    {
        HMODULE hModules = NULL;
        DWORD dwModuleCount = 0;

        // E usamos a fun��o abaixo somente para retornar a quantidade de bytes necess�ria para o vetor de HMODULE's.
        // Com isso, descobrimos quantos m�dulos foram carregados pelo processo.
        CWA(psapi, EnumProcessModules)(hProcess, &hModules, sizeof(hModules), &dwModuleCount);

		// Fechamos o handle para o processo aberto.
        CWA(kernel32, CloseHandle)(hProcess);

		// Isso faz retornar a quantidade exata de m�dulos carregados.
        return dwModuleCount / sizeof(HMODULE);
    }

    return 0;
}

DWORD PsGetCurrentProcessModuleCount()
{
#if defined(MSVC)
    return PsGetProcessModuleCount(__readfsdword(0x20));
#elif defined(GCC)
    register DWORD dwResult = 0;
    asm
    (
        ".intel_syntax noprefix \n"
        "mov eax, fs:[0x20]     \n"
        ".att_syntax            \n"
        : "=r" (dwResult)
    );
	return dwResult;
#elif defined(BORLAND_C)
	register DWORD dwResult = 0;
	__asm
	{
		mov eax, dword ptr fs:[0x20]
        mov dwResult, eax
	}
	return dwResult;
#endif
}

HMODULE PsGetKernel32BaseAddress()
{
	static HMODULE hKernel32 = NULL;

	if (!hKernel32)
		hKernel32 = PsGetModuleBaseAddressW(L"kernel32");

	return hKernel32;
}
