#ifndef __PROCESS_GLOBAL_H__
#define	__PROCESS_GLOBAL_H__

#include <tlhelp32.h>
#include <psapi.h>

#ifdef GCC
#include <ddk/ntddk.h>
#else
#define STATUS_SUCCESS ((NTSTATUS)0x00000000L)
#include <winternl.h>
#endif // GCC

#endif // __PROCESS_GLOBAL_H__
