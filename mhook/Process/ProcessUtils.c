#include "..\Process.h"

DWORD PsGetPidByImage(LPCTSTR lpImageName)
{
	// Obt�m handle para snapshot de todos os processos ativos no host atual.
    HANDLE hSnapshot = CWA(kernel32, CreateToolhelp32Snapshot)(TH32CS_SNAPPROCESS, 0);
    if (hSnapshot)
    {
        PROCESSENTRY32 pe32; // Estrutura que recebe os dados de um processo obtido do snapshot anterior.
        pe32.dwSize = sizeof(PROCESSENTRY32);

		// Verificamos se j� o primeiro processo � o que procuramos.
        if (CWA(kernel32, Process32First)(hSnapshot, &pe32))
            if (CWA(kernel32, lstrcmpi)(pe32.szExeFile, lpImageName) == 0)
                return pe32.th32ProcessID; // Retorna PID do processo, caso seja este.

		// Percorre todos os processos obtidos no snapshot a procura do que queremos.
        while (CWA(kernel32, Process32Next)(hSnapshot, &pe32))
            if (CWA(kernel32, lstrcmpi)(pe32.szExeFile, lpImageName) == 0)
                return pe32.th32ProcessID; // Retorna PID do processo, caso seja este.

		// Fecha o handle do snapshot
        CWA(kernel32, CloseHandle)(hSnapshot);
    }

    return (DWORD)-1;
}

