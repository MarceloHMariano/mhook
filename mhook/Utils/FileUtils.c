#include "FileUtils.h"
#include "Memory.h"

size_t MReadFile(HANDLE hFile, void *lpBuffer, DWORD dwBytesToRead, long dwOffset, DWORD dwMoveMethod)
{
	DWORD dwBytesRead;

	MFileMove(hFile, dwOffset, dwMoveMethod);
	CWA(kernel32, ReadFile)(hFile, lpBuffer, dwBytesToRead, &dwBytesRead, NULL);

	return dwBytesRead == dwBytesToRead ? dwBytesRead : 0;
}

size_t MWriteFile(HANDLE hFile, const void *lpBuffer, DWORD dwBytesToWrite, long dwOffset, DWORD dwMoveMethod)
{
	DWORD dwBytesWritten;

	MFileMove(hFile, dwOffset, dwMoveMethod);
	CWA(kernel32, WriteFile)(hFile, lpBuffer, dwBytesToWrite, &dwBytesWritten, NULL);

	return dwBytesWritten == dwBytesToWrite ? dwBytesWritten : 0;
}

mbool MExpandFile(HANDLE hFile, size_t nSize)
{
	return MFileMoveFromEnd(hFile, nSize) != INVALID_SET_FILE_POINTER 
		&& CWA(kernel32, SetEndOfFile)(hFile);
}

static void MZeroFile(HANDLE hFile, unsigned long offset, size_t nSize)
{
	void *buffer = MemHAlloc(nSize);
	if (buffer)
	{
		MWriteFile(hFile, buffer, nSize, offset, FILE_BEGIN);
		MemHFree(buffer);
	}
}

mbool MShiftFileBytes(HANDLE hFile, unsigned long offset, size_t nByteCount, FileShiftType shift)
{
	register mbool result = false;

	switch (shift)
	{
		case fsLeft:
			// TODO: implementar
		break;

		case fsRight:
		{
			size_t size = CWA(kernel32, GetFileSize)(hFile, NULL) - offset;
			if (size > 0)
			{
				void *buffer = MemVAlloc(NULL, size, PAGE_READWRITE);
				if (buffer)
				{
					if (MReadFileAll(hFile, buffer, size, offset, FILE_BEGIN))
					{
						MZeroFile(hFile, offset, nByteCount);
						result = MWriteFileAll(hFile, buffer, size, offset + nByteCount, FILE_BEGIN);
					}

					MemVFree(buffer);
				}
			}
		}
		break;
	}

	return result;
}

void *MLoadFileInMemory(HANDLE hFile)
{
	if (hFile && hFile != INVALID_HANDLE_VALUE)
	{
		
	}

	return NULL;
}