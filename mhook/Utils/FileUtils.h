#ifndef __PE_UTILS_H__
#define	__PE_UTILS_H__

#include "common.h"

#define	MFileMove(hFile, offset, method)		CWA(kernel32, SetFilePointer)((HANDLE)hFile, offset, NULL, method)
#define	MFileMoveFromBegin(hFile, offset)		MFileMove(hFile, offset, FILE_BEGIN)
#define	MFileMoveFromCurrent(hFile, offset)		MFileMove(hFile, offset, FILE_CURRENT)
#define	MFileMoveFromEnd(hFile, offset)			MFileMove(hFile, offset, FILE_END)

#define MReadFileAll(hFile, lpBuffer, dwBytesToRead, offset, dwMoveMethod) \
	MReadFile(hFile, lpBuffer, dwBytesToRead, offset, dwMoveMethod) == dwBytesToRead

#define	MWriteFileAll(hFile, lpBuffer, dwBytesToWrite, offset, dwMoveMethod) \
	MWriteFile(hFile, lpBuffer, dwBytesToWrite, offset, dwMoveMethod) == dwBytesToWrite

typedef enum {fsLeft = 0, fsRight} FileShiftType;

mbool MExpandFile(HANDLE hFile, size_t nSize);
mbool MShiftFileBytes(HANDLE hFile, unsigned long offset, size_t nByteCount, FileShiftType shift);
size_t MReadFile(HANDLE hFile, void *lpBuffer, DWORD dwBytesToRead, long dwOffset, DWORD dwMoveMethod);
size_t MWriteFile(HANDLE hFile, const void *lpBuffer, DWORD dwBytesToWrite, long dwOffset, DWORD dwMoveMethod);
void *MLoadFileInMemory(HANDLE hFile);

#endif // __PE_UTILS_H__