#ifndef __CORE_DEFS_H__
#define	__CORE_DEFS_H__

#include "defs.h"

#ifdef __cplusplus
#define	CWA(dll, api)	::api
#else
#define	CWA(dll, api)	api
#endif

#define	MAKE_STRING(ident)	#ident
// GWA: Get Windows API
#define	GWA(funcname) MAKE_STRING(funcname)

#ifdef UNICODE
typedef wchar_t mchar;
#define	mT(text)	L##text
#else
typedef char mchar;
#define	mT(text)	text
#endif // _UNICODE

typedef unsigned char mbool;
#define	true	(mbool)1
#define	false	(mbool)0

typedef void *mhandle_t;
typedef unsigned char mbyte;

#endif // __CORE_DEFS_H__
