#pragma once

#ifndef __DEFS_H__
#define	__DEFS_H__

#if defined(__GNUC__) && !defined(_MSC_VER)
#define GCC
#elif defined(_MSC_VER) && !defined(__GNUC__)
#define MSVC
#elif defined(__BORLANDC__)
#define	BORLAND_C
#else
#error	"Impossível determinar compilador usado."
#endif

#ifdef __cplusplus
#define	EXPORT_AS_C()		extern "C" {
#define	EXPORT_AS_C_END()	}
#else
#define	EXPORT_AS_C()
#define	EXPORT_AS_C_END()
#endif // __cplusplus

#if defined(GCC)
#define	NAKED	__attribute((naked))
#else
#define	NAKED	__declspec(naked)
#endif

#endif // __DEFS_H__